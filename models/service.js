const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create geolocation Schema model
const GeoSchema = new Schema({
  type: {
    type:String,
    default: "Point"
    },
   coordinates: {
    type: [Number],
    index: "2dsphere"
  }
});

//create Service schema model
const ServiceSchema = new Schema({

  outlet_name:{
    type:String,
    required:[true, "outlet_name is required"]
  },
  service_centre_name:{
    type:String,
    required:[true, "service_centre_name is required"]
  },

  owns_a_smartphone:{
    type:Boolean,
    default:false
  },
  phone_number:{
    type:Number
  },
  alternate_phone_number:{
    type:Number
  },
  opening_time:{
    type:String
  },
  closing_time:{
    type:String
  },
  established:{
    type:Number
  },
  type_of_vehicles_serviced:{
    type:String
  },
  Make_of_vehicles_serviced:{
    type:String
  },
  type_of_service:{
    type:String
  },
  services_available:{
    type:String
  },
geometry: GeoSchema
});

const Service = mongoose.model('service',ServiceSchema);//service is collection

module.exports = Service;
