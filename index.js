const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// set up express app
const app = express();

// connect to mongodb so that operations can be performed on our database
mongoose.connect('mongodb://localhost/servicego');
mongoose.Promise = global.Promise;

app.use(express.static('public'));
// use body-parser middleware
app.use(bodyParser.json());

// initialize routes
app.use('/api', require('./routes/api'));

// error handling middleware and sends error message
app.use(function(err, req, res, next){
   //console.log(err); // to see properties of message in our console
  res.status(422).send({error: err.message});
});
// listen for http requests we are making
app.listen(process.env.port || 4000, function(){ // we are using localhost:4000
    console.log('now listening for requests');//this will be displayed in our command prompt when we run i(node index) in our command prompt
});
