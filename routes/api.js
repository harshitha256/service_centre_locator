const express = require ('express');
const router = express.Router();
const Service = require('../models/service');

//get  a list of service centres from the db
router.get('/services', function(req, res, next){
    /* Ninja.find({}).then(function(ninjas){
        res.send(ninjas);
    }); */
    Service.aggregate().near({
        near:{type: 'Point', coordinates: [parseFloat(req.query.lng), parseFloat(req.query.lat)]},
        distanceField:"dist.calculated",
        maxDistance:100000,
        spherical:true
    }).then(function(services){
        res.send(services);
    }).catch(next);
});

//add a new service centre to the db
router.post('/services', function(req, res, next){
  /*var service = new Service(req.body);
  service.save();*/
 Service.create(req.body).then(function(service){
   res.send(service);
  }).catch(next);
});


//update a service_centre in the db
router.put('/services/:id', function(req, res, next){
 Service.findByIdAndUpdate({_id:req.params.id},req.body).then(function(service){
   Service.findOne({_id:re.params.id}).then(function(service){
    res.send({type :'PUT'});
   });
 });
});


//delete a service centre from the db using _id
router.delete('/services/:id', function(req, res, next){
  Service.findByIdAndRemove({_id:req.params.id}).then(function(service){
    res.send(service);
  });
 });



module.exports = router;
